import React from 'react';
import logo from '../assets/image/logo.svg';
import './App.css';

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import { Container } from '@material-ui/core';
import Header from './layout/header/Header';
import CarouselHome from './carousel/CarouselHome';
import Product from './product/Product';
import Comment from './comment/Comment';


function App() {
  return (
    <div>
      <Header></Header>
      <CarouselHome></CarouselHome>
      <Product></Product>
      {/* <Comment></Comment> */}
    </div>
  );
}

export default App;
