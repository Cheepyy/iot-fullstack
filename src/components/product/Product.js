import React from "react";
import "./Product.css";

import Card from "../../helpers/Card";
import Mirror from "../../assets/image/Mirror.jpg";
import Watch from "../../assets/image/montre.jpg";
import Belt from "../../assets/image/ceinture.jpg";

export default function Product() {
  const desc =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eleifend blandit mauris sit amet dignissim. Vestibulum vitae sem dolor. Aliquam tempor ac tortor quis convallis. ";
  return (
    <div class="container">
      <div class="productTitle">
        <h2 id="productTitleText">Produits</h2>
      </div>
      <div class="productCardContainer">
        <Card image={Mirror} title={"Mirror"} alt={"Mirror"} desc={desc}></Card>
        <Card
          image={Watch}
          title={"Montre connectée"}
          alt={"Montre connectée"}
          desc={desc}
        ></Card>
        <Card
          image={Belt}
          title={"Ceinture connectée"}
          alt={"Ceinture connectée"}
          desc={desc}
        ></Card>
      </div>
    </div>
  );
}
