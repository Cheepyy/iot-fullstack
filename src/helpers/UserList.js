import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';

import User1 from "../assets/image/user1.png";
import User2 from "../assets/image/user2.png";
import User3 from "../assets/image/user3.png";

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: '100%',
    backgroundColor: theme.palette.background.paper,
    paddingTop: "5%"
  },
}));

export default function FolderList() {
  const classes = useStyles();

  return (
    <List className={classes.root}>
      <ListItem>
        <ListItemAvatar>
          <Avatar src={User1}>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Garfield" secondary="Une perte de poids imprésionnante" />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar src={User2}>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Julien" secondary="Je n'aurai pu demandé mieux" />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar src={User3}>
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Alexia" secondary="Intuitif, solide, pratique." />
      </ListItem>
    </List>
  );
}